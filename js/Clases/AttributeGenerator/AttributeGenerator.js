import ThrowDices from '../ThrowDices/ThrowDices.js';

export default class AttributeGenerator {
    /** 
    *   Clase: Esta clase es la encargada de generar las estadisticas iniciales del personaje a las que despues se aplicaran los
    *     modificadores.
    */

    _roll4x6(){
      /**
      *   Método(rivado)
      *
      *   Realiza
      *       Ejecuta 4 lanzamientos de dados de 6 caras.
      *       
      */
      const throwDicesd6  = new ThrowDices(6);
      let result = 0;
      for(let i=0; i < 4; i++){
        result = parseInt(result) + parseInt(throwDicesd6.throw);
      }
      return result;
    }

    _roll4x20(){
      /**
      *   Método(rivado)
      *
      *   Realiza
      *       Ejecuta 4 lanzamientos de dados de 20 caras.
      *       
      */
      const throwDicesd20  = new ThrowDices(20);
      let result = 0;
      for(let i=0; i < 4; i++){
        result = parseInt(result) + parseInt(throwDicesd20.throw);
      }
      return result;
    }

    get pjAttributes(){
      /**
      *   Getter
      *
      *   Realiza
      *       Devuelve las estadisticas iniciales del personaje generadas aleatoriamente.
      *       
      */
      const str = this._roll4x6();
      const con = this._roll4x6();
      const dex = this._roll4x6();
      const int = this._roll4x6();
      const wis = this._roll4x6();
      const cha = this._roll4x6();
      const vit = 100 + parseInt(parseInt (con) * 10) + parseInt(this._roll4x20());
      const sta = 100 + parseInt(this._roll4x20());
      return {str: str, con: con, dex: dex, int: int, wis: wis, cha: cha, vit: vit, sta: sta};
    }
}