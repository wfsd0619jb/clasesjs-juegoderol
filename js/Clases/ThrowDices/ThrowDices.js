export default class ThrowDices{
    /* 
    *   Clase: Esta clase es la encargada de realizar las tiradas de dados en función de la cantidad de caras que reciba como atributo.
    */
    constructor(faces){
        this.faces = faces;
    }

    get throw(){
        return Math.round(Math.random()*(parseInt(this.faces)-1)+parseInt(1));
    }
}