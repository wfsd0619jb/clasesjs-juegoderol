import Heroe from '../Heroe.js';

export default class Enano extends Heroe {
    constructor(pjName, pjRace, pjClass, pjAttributtes) {
        super(pjName, pjRace, pjClass, pjAttributtes);
        this._str = parseInt(this._str) + 2;
        this._con = parseInt(this._con) + 2;
        this._dex = parseInt(this._dex) - 1;
        this._wis = parseInt(this._wis) + 2;
    }
}